package pt.ipl.estg.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Utils {
	public Properties loadProperties(String confFilePath) throws IOException{
		Properties prop = new Properties();
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(confFilePath);
		if(inputStream!=null){
			prop.load(inputStream);
		}else{
			throw new FileNotFoundException("property file " + confFilePath + " not found in classpath");
		}
		return prop;
	}

}
