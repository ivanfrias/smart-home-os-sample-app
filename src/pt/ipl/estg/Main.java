package pt.ipl.estg;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.apache.log4j.Logger;


/***
 * This classes is the entry point for the Importer Program
 * 
 * @author ivanfrias
 * 
 */
public class Main {

	private static final Logger log = Logger.getLogger(Main.class);
	
	private static final String IP = "127.0.0.1";
	private static final int PORT = 1024; 
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		if(args.length==1){
			try{
				Socket socket = new Socket(IP, PORT);
				PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
				BufferedReader in =new BufferedReader(new InputStreamReader(socket.getInputStream()));
				writer.println(args[0]);
				String devices = in.readLine();
				System.out.println("Connected devices " + devices);
			}catch(Exception e){
				e.printStackTrace();
				log.error(e);
			}
		}else{
			System.out.println("Error !");
			System.exit(0);
		}
		

	}

}
